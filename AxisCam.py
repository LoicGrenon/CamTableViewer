# -*- coding: utf-8 -*-

from BoschXmlCamParser import BoschXmlCamParser
from scipy import interpolate
import numpy as np


class AxisCam:
    def __init__(self, x=None, y=None):
        self.x = x if x else []
        self.y = y if y else []
        self.data = []

        self._interpolX, self._interpolY = self.setInterpolator()

        xLength = len(self.x)
        if xLength:
            self.xMin = min(self.x)
            self.xMax = max(self.x)
        else:
            self.xMin = None
            self.xMax = None

        yLength = len(self.y)
        if yLength:
            self.yMin = min(self.y)
            self.yMax = max(self.y)
        else:
            self.yMin = None
            self.yMax = None

        if xLength == yLength:
            self.len = xLength
        else:
            raise ValueError("Axis x-values and y-values should have the same length")

    def importFromBoschXml(self, xmlFile):
        parser = BoschXmlCamParser(xmlFile)
        self.data = parser.parse()
        self.x, self.y = parser.getSlavePositions()

        self.xMin = min(self.x)
        self.xMax = max(self.x)

        self.yMin = min(self.y)
        self.yMax = max(self.y)

        self.len = len(self.y)

        self.setInterpolator()

    def toArray(self):
        return np.array((self.x, self.y))

    def getRange(self, y1, y2):
        array = self.toArray()

        if y1 is not None and y2 is not None:
            condition = np.logical_and(array[1] > y1, array[1] < y2)
        elif y1 is None:
            condition = array[1] < y2
        else:
            condition = array[1] > y1

        prev = None
        start = 0
        rangeLimits = []
        for i, value in enumerate(condition):
            if value != prev:
                if value:  # beginning pos is found
                    start = i
                elif prev is not None:  # ending pos is found
                    rangeLimits.append((array[0][start], array[0][i - 1]))
                prev = value
        if prev is not None and value:
            rangeLimits.append((array[0][start], array[0][i]))
        return rangeLimits

    def setInterpolator(self, interpolator=interpolate.interp1d):
        interpX = self.setInterpolatorForX(interpolator)
        interpY = self.setInterpolatorForY(interpolator)
        return interpX, interpY

    def setInterpolatorForX(self, interpolator=interpolate.interp1d):
        """
        Define the interpolator to use to approximate the axis cam positions
        The length of returned list depends on the number of monotonic parts
        :param interpolator: interpolator function to use, default is scipy.interpolate.interp1d
        :return: a list of interpolator functions to approximate x=f(y)
        """
        #  See https://stackoverflow.com/questions/48028766

        if not len(self.x) or not len(self.y):
            return
        # convert data lists to arrays
        x, y = np.array(self.x), np.array(self.y)

        # sort x and y by x value
        order = np.argsort(x)
        xSort, ySort = x[order], y[order]

        # compute indices of points where y changes direction
        yDirection = np.sign(np.diff(ySort))
        changePoints = 1 + np.where(np.diff(yDirection) != 0)[0]

        # find groups of x and y within which y is monotonic
        xGroups = np.split(xSort, changePoints)
        yGroups = np.split(ySort, changePoints)
        interps = [interpolator(y, x, bounds_error=False) for y, x in zip(yGroups, xGroups)]
        self._interpolX = interps
        return interps

    def setInterpolatorForY(self, interpolator=interpolate.interp1d):
        """
        Define the interpolator to use to approximate the axis cam positions
        :param interpolator: interpolator function to use, default is scipy.interpolate.interp1d
        :return: the interpolator function to approximate y=f(x)
        """
        if not len(self.x) or not len(self.y):
            return None
        with np.errstate(divide='ignore', invalid='ignore'):  # silent the warnings caused by the interpolator
            self._interpolY = interpolator(self.x, self.y)
        return self._interpolY

    def getX(self, yValue):
        """
        Return a list of x-values corresponding to a y-value using the interpolator
        :param yValue: y-value we want to know the corresponding x-value
        :return: a list of x-values corresponding to the given y-value
        """
        if yValue < self.yMin:
            raise ValueError("value should be greater than the minimum y-value")
        elif yValue > self.yMax:
            raise ValueError("value should be lesser than the maximum y-value")
        return [float(interp(yValue)) for interp in self._interpolX if not np.isnan(interp(yValue))]

    def getY(self, value):
        """
        Return a y-value corresponding to a x-value using the interpolator
        :param value: x-value we want to know the corresponding y-value
        :return: the y-value corresponding to the given x-value
        """
        if value < self.xMin:
            raise ValueError("value should be greater than the minimum x-value")
        elif value > self.xMax:
            raise ValueError("value should be lesser than the maximum x-value")
        return float(self._interpolY(value))
