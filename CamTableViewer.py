# -*- coding: utf-8 -*-

from AxisCam import AxisCam
from pathlib import Path
from PySide import QtCore, QtGui
from ui.main import Ui_MainWindow
from operator import itemgetter
import pyqtgraph as pg
import pyqtgraph.exporters
import random
import uuid
import yaml

AXIS_NAME_COL = 0
AXIS_ID_ROLE = QtCore.Qt.UserRole
AXIS_CAM_ROLE = QtCore.Qt.UserRole + 1
AXIS_LOAD_BTN_COL = 1
AXIS_SHOW_BTN_COL = 2

CAM_NAME_COL = 0
CAM_BEGIN_COL = 1
CAM_END_COL = 2
CAM_AXIS_COL = 3
CAM_COLOR_COL = 4


def getRandomColor():
    colorList = QtGui.QColor.colorNames()
    colorList.remove('black')
    return colorList[random.randint(0, len(colorList))]


class CamTableViewer(QtGui.QMainWindow):
    def __init__(self):
        super(CamTableViewer, self).__init__()

        self.ui = Ui_MainWindow()
        self.setupUi()
        self._openedFile = None
        self.plotSigProxy = None

    def setupUi(self):
        # Setup UI created with Qt Designer
        self.ui.setupUi(self)

        ####################
        # Axis tableWidget #
        ####################
        self.ui.axis_tableWidget.setColumnCount(4)
        self.ui.axis_tableWidget.setHorizontalHeaderLabels(("Name",
                                                            "",
                                                            "",
                                                            ""))
        self.ui.axis_tableWidget.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        header = self.ui.axis_tableWidget.horizontalHeader()
        # header.setResizeMode(QtGui.QHeaderView.Stretch)
        header.setResizeMode(AXIS_NAME_COL, QtGui.QHeaderView.Stretch)
        header.setStretchLastSection(True)
        self.ui.axis_tableWidget.itemChanged.connect(self.onAxisChanged)

        ####################
        # Cams tableWidget #
        ####################
        self.ui.cams_tableWidget.setColumnCount(5)
        self.ui.cams_tableWidget.setHorizontalHeaderLabels((u"Name",
                                                            u"Start pos.",
                                                            u"End pos.",
                                                            u"Axis",
                                                            u"Color"))
        self.ui.cams_tableWidget.setSelectionBehavior(QtGui.QAbstractItemView.SelectRows)
        header = self.ui.cams_tableWidget.horizontalHeader()
        header.setResizeMode(QtGui.QHeaderView.ResizeToContents)
        header.setResizeMode(CAM_NAME_COL, QtGui.QHeaderView.Stretch)
        header.setResizeMode(CAM_AXIS_COL, QtGui.QHeaderView.Stretch)

        ########################
        # graphicView settings #
        ########################
        yax = self.ui.graphicsView.getAxis('left')
        yax.setTicks([])

        xax = self.ui.graphicsView.getAxis('bottom')
        xax.setTickSpacing(20, 20)

        plot = self.ui.graphicsView.getPlotItem()
        plot.showGrid(True, True, 0.3)
        # Disable mouse interaction
        plot.setMouseEnabled(False, False)
        plot.setMenuEnabled(False)
        plot.setXRange(0, 365, 0)  # Set view limits

        self.ui.dockWidget.setFloating(True)
        self.ui.dockWidget.setFixedSize(900, 225)
        self.ui.dockWidget.hide()

        #############################
        # Signals/Slots connections #
        #############################
        self.ui.addAxis_btn.clicked.connect(self.addAxisTableRow)
        self.ui.delAxis_btn.clicked.connect(self.delAxisTableRow)
        self.ui.trace_btn.clicked.connect(self.showCamGraph)
        self.ui.exportPlot_btn.clicked.connect(self.exportPlot)
        self.ui.closePlot_btn.clicked.connect(self.ui.dockWidget.close)
        self.ui.addCam_btn.clicked.connect(self.addCamTableRow)
        self.ui.delCam_btn.clicked.connect(self.delCamTableRow)

        self.ui.actionOpen.triggered.connect(self.actionOpen_triggered)
        self.ui.actionSave.triggered.connect(lambda: self.save(self._openedFile))
        self.ui.actionSave_As.triggered.connect(self.save)
        self.ui.actionQuit.triggered.connect(self.close)

    def clear(self):
        self.ui.axis_tableWidget.clearContents()
        self.ui.axis_tableWidget.setRowCount(0)  # Clear QTableWidget
        self.ui.cams_tableWidget.clearContents()
        self.ui.cams_tableWidget.setRowCount(0)  # Clear QTableWidget
        self.ui.graphicsView.clear()

    def actionOpen_triggered(self):
        filename, _ = QtGui.QFileDialog.getOpenFileName(self.ui.centralwidget,
                                                        caption="Open file",
                                                        filter="Cam files (*.yaml)")
        if not filename:
            return False
        try:
            with open(filename, "r") as f:
                self._openedFile = filename
                try:
                    data = yaml.safe_load(f)
                except UnicodeDecodeError as e:
                    QtGui.QMessageBox.error(self.ui.centralwidget,
                                            "File open",
                                            f"The file {filename} do not seems to be a valid cam file.\n"
                                            f"Returned error is : {e}")
                    return False
                except yaml.scanner.ScannerError as e:
                    QtGui.QMessageBox.error(self.ui.centralwidget,
                                            "File open",
                                            f"Unable to read cam file {filename}.\nReturned error is : {e}")
                    return False
        except (IOError, OSError) as e:
            QtGui.QMessageBox.error(self.ui.centralwidget,
                                    "File open",
                                    f"Unable to read cam file {filename}.\n error is : {e}")
            return False

        self.clear()
        self.loadAxisData(data)  # Read data and populate axis QTableWidget
        self.loadCamData(data)  # Read data and populate cams QTableWidget
        self.showCamGraph()
        return True

    def loadAxisData(self, data):
        if "Axis" not in data:
            return

        for a in sorted(data['Axis'], key=itemgetter('id')):  # iterate over the dict list by axis id in ascending order
            try:
                axisName = str(a['name'])
            except (KeyError, ValueError, TypeError):
                continue

            xVal = a['x']
            yVal = a['y']
            try:
                ac = AxisCam(xVal, yVal)
            except ValueError:
                continue

            self.addAxisTableRow(ac, axisName, editOnInsert=False)

    def loadCamData(self, data):
        if 'Cams' not in data:
            return

        for c in data['Cams']:
            cam = dict()
            try:
                cam['name'] = str(c['name'])
                cam['begin'] = "" if c['begin'] is None or c['begin'] == "" else int(c['begin'])
                cam['end'] = "" if c['end'] is None or c['end'] == "" else int(c['end'])
                cam['color'] = str(c['color']) if self._isValidColorName(c['color']) else getRandomColor()
                cam['axis'] = int(c['axis'])
            except (KeyError, ValueError, TypeError):
                continue

            self.addCamTableRow(cam['name'], cam['begin'], cam['end'], cam['color'], cam['axis'], editOnInsert=False)

    def _isValidColorName(self, color):
        if color in QtGui.QColor.colorNames():
            return True
        else:
            # Regex for CSS colors: hex, rgb(a), hsl(a)
            # https://gist.github.com/olmokramer/82ccce673f86db7cda5e
            regex = r"(#(?:[0-9a-f]{2}){2,4}|(#[0-9a-f]{3})|(rgb|hsl)a?\((-?\d+%?[,\s]+){2,3}\s*[\d\.]+%?\))"
            rx = QtCore.QRegExp(regex)
            v = QtGui.QRegExpValidator(rx, self)
            return v.validate(color, 0)[0] == QtGui.QRegExpValidator.Acceptable

    def save(self, filename=None):
        data = None
        camData = self.getCamTable()
        axisData = self.getAxisTable()
        if camData and axisData:
            data = dict(camData, **axisData)  # Combine the two dicts
        elif camData:
            data = camData
        elif axisData:
            data = axisData

        if not data:
            return False
        elif not filename:
            filename, _ = QtGui.QFileDialog.getSaveFileName(self.ui.centralwidget,
                                                            caption="Save as ...",
                                                            filter="Cam files (*.yaml)")
            if not filename:
                return False

        try:
            with open(filename, "w+") as f:
                yaml.dump(data, f)
                self._openedFile = filename
                QtGui.QMessageBox.information(self.ui.centralwidget,
                                              "File save",
                                              f"File successfully saved to {filename}.")
        except (IOError, OSError) as e:
            QtGui.QMessageBox.error(self.ui.centralwidget,
                                    "File save",
                                    f"Unable to save to file {filename}.\nReturned error is : {e}")
            return

    def addAxisTableRow(self, ac=None, AxisName="", editOnInsert=True):
        if not ac:
            filename, _ = QtGui.QFileDialog.getOpenFileName(filter="XML files (*.xml)")
            if filename:
                ac = AxisCam()
                ac.importFromBoschXml(filename)
                AxisName = Path(filename).stem

        if ac:
            table = self.ui.axis_tableWidget
            lastRow = table.rowCount()
            table.insertRow(lastRow)

            # Name column
            item = QtGui.QTableWidgetItem(AxisName)
            item.setTextAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
            item.setData(AXIS_ID_ROLE, uuid.uuid1().hex)  # set axis id
            item.setData(AXIS_CAM_ROLE, ac)
            table.setItem(lastRow, AXIS_NAME_COL, item)
            if editOnInsert:
                table.editItem(item)

            # Load values button column
            btn = QtGui.QPushButton("Load cam")
            btn.clicked.connect(self.loadAxisValues)
            table.setCellWidget(lastRow, AXIS_LOAD_BTN_COL, btn)

            # Show button column
            btn = QtGui.QPushButton("Show")
            btn.clicked.connect(self.showAxis)
            table.setCellWidget(lastRow, AXIS_SHOW_BTN_COL, btn)

    def delAxisTableRow(self):
        table = self.ui.axis_tableWidget
        selectedRow = {idx.row() for idx in table.selectedIndexes()}
        for row in reversed(list(selectedRow)):
            table.removeRow(row)

        self.onAxisChanged()

    def loadAxisValues(self, newCam=False):
        table = self.ui.axis_tableWidget
        if newCam:
            row = table.rowCount()
        else:
            buttonClicked = self.sender()
            idx = table.indexAt(buttonClicked.pos())
            row = idx.row()

        filename, _ = QtGui.QFileDialog.getOpenFileName(filter="XML files (*.xml)")
        if not filename:
            return False

        ac = AxisCam()
        ac.importFromBoschXml(filename)
        table.item(row, AXIS_NAME_COL).setData(AXIS_CAM_ROLE, ac)
        return True

    def showAxis(self):
        buttonClicked = self.sender()
        idx = self.ui.axis_tableWidget.indexAt(buttonClicked.pos())
        item = self.ui.axis_tableWidget.item(idx.row(), AXIS_NAME_COL)
        ac = item.data(AXIS_CAM_ROLE)

        if isinstance(ac, AxisCam):
            win = pg.GraphicsWindow(item.text())

            label = pg.LabelItem(justify='right')
            win.addItem(label)

            p1 = win.addPlot(row=1, col=0)
            p1.plot(x=ac.x, y=ac.y, pen="r")
            p1.showGrid(x=True, y=True, alpha=0.4)

            # cross hair
            vLine = pg.InfiniteLine(angle=90, movable=False)
            hLine = pg.InfiniteLine(angle=0, movable=False)
            p1.addItem(vLine, ignoreBounds=True)
            p1.addItem(hLine, ignoreBounds=True)

            # Slot triggered on mouse move event over the plot widget
            def mouseMoved(evt):
                pos = evt[0]  # using signal proxy turns original arguments into a tuple
                mousePoint = p1.vb.mapSceneToView(pos)
                vLine.setPos(mousePoint.x())
                hLine.setPos(mousePoint.y())
                if p1.sceneBoundingRect().contains(pos) and ac.xMin <= mousePoint.x() <= ac.xMax:
                    label.setText(f"<span>Mouse: ({mousePoint.x():.2f}; {mousePoint.y():.2f})</span><br>"
                                  f"<span>Plot: ({mousePoint.x():.2f}; {ac.getY(mousePoint.x()):.2f})</span>")

            # signal proxy need to be a class member
            self.plotSigProxy = pg.SignalProxy(p1.scene().sigMouseMoved, rateLimit=60, slot=mouseMoved)
        else:
            QtGui.QMessageBox.warning(self.ui.centralwidget,
                                      "No axis cam values loaded yet",
                                      "You should load axis cam values first by clicking on the 'Load cam' button.")

    def getAxisNames(self):
        axisNames = []
        table = self.ui.axis_tableWidget
        for rowId in range(table.rowCount()):
            if self.isValidAxis(rowId):
                item = table.item(rowId, AXIS_NAME_COL)
                axisNames.append({'id': item.data(AXIS_ID_ROLE),
                                  'name': item.text().strip()})
        return axisNames

    def isValidAxis(self, axisRowId):
        """

        :param axisRowId:
        :return: True if all axis parameters are given, False otherwise
        """
        axisName = self.ui.axis_tableWidget.item(axisRowId, AXIS_NAME_COL).text()
        return len(axisName.strip()) > 0

    def getAxisTableRow(self, axis_uuid):
        """
        Return the row id for a given axis UUID
        :param axis_uuid:
        :return:
        """
        table = self.ui.axis_tableWidget
        for rowId in range(table.rowCount()):
            if table.item(rowId, AXIS_NAME_COL).data(AXIS_ID_ROLE) == axis_uuid:
                return rowId

    def onAxisChanged(self):
        table = self.ui.cams_tableWidget
        for rowId in range(table.rowCount()):
            axis_cb = table.cellWidget(rowId, CAM_AXIS_COL)
            # Iterate over the list in reversed order since we may remove combobox items while iterating on it
            for itemId in reversed(range(2, axis_cb.count())):
                axisId = axis_cb.itemData(itemId)
                # Check if axis UUID is in axis list
                res = [d['name'] if ("id", axisId) in d.items() else False for d in self.getAxisNames()]
                if not any(res):  # Current item UUID no longer exists => remove it
                    if itemId == axis_cb.currentIndex():
                        axis_cb.setCurrentIndex(0)  # Deselect the current axis from the combobox
                        axis_cb.removeItem(itemId)
                else:  # Current item UUID still exists => update it
                    res = set(res)
                    if False in res:
                        res.remove(False)
                    axis_cb.setItemText(itemId, res.pop())

    def getAxisTable(self):
        axis = []
        axisTable = self.ui.axis_tableWidget
        for rowId in range(axisTable.rowCount()):
            item = axisTable.item(rowId, AXIS_NAME_COL)
            name = item.text()
            axis_cam = item.data(AXIS_CAM_ROLE)

            axis.append({"name": name,
                         "id": rowId + 1,  # rowId = 0 and 1 are respectively reserved to No axis selected and VM axis
                         "x": axis_cam.x,  # axis x-values list
                         "y": axis_cam.y   # axis y-values list
                         })
        return {"Axis": axis} if len(axis) else None

    def getCamTable(self):
        cams = []
        for rowId in range(self.ui.cams_tableWidget.rowCount()):
            try:
                name = self.ui.cams_tableWidget.item(rowId, CAM_NAME_COL).text()
            except AttributeError:
                name = ""
            try:
                begin = int(self.ui.cams_tableWidget.item(rowId, CAM_BEGIN_COL).text())
            except (AttributeError, ValueError):
                begin = None
            try:
                end = int(self.ui.cams_tableWidget.item(rowId, CAM_END_COL).text())
            except (AttributeError, ValueError):
                end = None
            colorCellWidget = self.ui.cams_tableWidget.cellWidget(rowId, CAM_COLOR_COL)
            color = colorCellWidget.palette().color(QtGui.QPalette.Background).name()
            axisId = self.ui.cams_tableWidget.cellWidget(rowId, CAM_AXIS_COL).currentIndex()

            if name is not None:
                cams.append({"name": name,
                             "begin": begin,
                             "end": end,
                             "color": color,
                             "axis": axisId})

        return {"Cams": cams} if len(cams) else None

    def addCamTableRow(self, name=None, begin=None, end=None, color=None, axisIdx=1, editOnInsert=True):
        table = self.ui.cams_tableWidget
        lastRow = table.rowCount()
        table.insertRow(lastRow)

        # Name and visibility column
        if name is None:
            name = ""
        item = QtGui.QTableWidgetItem(str(name))
        item.setCheckState(QtCore.Qt.Checked)
        item.setTextAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        table.setItem(lastRow, CAM_NAME_COL, item)
        if editOnInsert:
            table.editItem(item)

        # Start position column
        if begin is None:
            begin = ""
        item = QtGui.QTableWidgetItem(str(begin))
        item.setTextAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
        table.setItem(lastRow, CAM_BEGIN_COL, item)

        # End position column
        if end is None:
            end = ""
        item = QtGui.QTableWidgetItem(str(end))
        item.setTextAlignment(QtCore.Qt.AlignCenter | QtCore.Qt.AlignVCenter)
        table.setItem(lastRow, CAM_END_COL, item)

        # Axis column
        cb = self._createAxisCombobox()
        table.setCellWidget(lastRow, CAM_AXIS_COL, cb)
        cb.setCurrentIndex(axisIdx)

        # Color column
        colorPickerBtn = QtGui.QPushButton("")
        colorPickerBtn.setStyleSheet(f"background-color: {getRandomColor() if color is None else color}")
        # Add signal to show the color picker related to current row when the button is clicked
        colorPickerBtn.clicked.connect(self.colorPicker)

        table.setCellWidget(lastRow, CAM_COLOR_COL, colorPickerBtn)
        table.setItem(lastRow, CAM_COLOR_COL, QtGui.QTableWidgetItem())

    def delCamTableRow(self):
        table = self.ui.cams_tableWidget
        selectedRow = {idx.row() for idx in table.selectedIndexes()}
        for row in reversed(list(selectedRow)):
            table.removeRow(row)

    def _createAxisCombobox(self):
        cb = QtGui.QComboBox()
        cb.addItem("")
        cb.addItem("VM axis")
        for axis in self.getAxisNames():
            cb.addItem(axis['name'], axis['id'])  # Axis UUID is stored in the Qt.UserRole
        return cb

    def colorPicker(self):
        """
        Show a color picker (QColorDialog) and store the new selected color
        :return:
        """
        rowId = self.ui.cams_tableWidget.indexAt(self.sender().pos()).row()
        colorCellWidget = self.ui.cams_tableWidget.cellWidget(rowId, CAM_COLOR_COL)
        currentColor = colorCellWidget.palette().color(QtGui.QPalette.Background)
        newColor = QtGui.QColorDialog.getColor(currentColor, self)
        if newColor.isValid():
            button = self.ui.cams_tableWidget.cellWidget(rowId, CAM_COLOR_COL)
            button.setStyleSheet(f"background-color: {newColor.name()}")

    def getCamList(self):
        table = self.ui.cams_tableWidget

        camList = []
        for row in range(table.rowCount()):
            cam = dict()

            camVisibility = table.item(row, CAM_NAME_COL).checkState() == QtCore.Qt.Checked
            if not camVisibility:
                continue

            try:
                cam['name'] = table.item(row, CAM_NAME_COL).text()
                cam['color'] = table.cellWidget(row, CAM_COLOR_COL).palette().color(QtGui.QPalette.Background).name()
            except ValueError:
                continue

            try:
                cam['begin'] = int(table.item(row, CAM_BEGIN_COL).text())
            except ValueError:
                cam['begin'] = None

            try:
                cam['end'] = int(table.item(row, CAM_END_COL).text())
            except ValueError:
                cam['end'] = None

            if cam['begin'] is None and cam['end'] is None:
                continue

            axis_cb = table.cellWidget(row, CAM_AXIS_COL)
            axisId = axis_cb.currentIndex()
            if axisId == 0:
                continue
            elif axisId == 1:
                cam['axisCam'] = None
            else:
                axis_uuid = axis_cb.itemData(axisId)
                axisRowId = self.getAxisTableRow(axis_uuid)
                if axisRowId is None:
                    continue
                cam['axisCam'] = self.ui.axis_tableWidget.item(axisRowId, AXIS_NAME_COL).data(AXIS_CAM_ROLE)
            camList.append(cam)

        # The list is reversed since the cam graph y-axis is in negative direction
        # this way, the order in the table and the graph are the same
        return reversed(camList)

    def showCamGraph(self):
        camList = self.getCamList()

        self.ui.graphicsView.clear()

        yAxisNames = []  # used to display cam names as y-axis values
        for camId, cam in enumerate(camList):
            try:
                camBegin = cam['begin']
                camEnd = cam['end']
                camName = cam['name']
                try:
                    color = QtGui.QColor(cam['color'])
                except KeyError:
                    color = QtGui.QColor("")
                camAxis = cam['axisCam']
            except KeyError as e:
                QtGui.QMessageBox.error(self.ui.centralwidget,
                                        "Trace cam graph",
                                        f"Incorrect cam description : {e} key missing - {cam}")
                continue

            if camAxis is None:  # Cam is related to master virtual axis
                if camBegin is None:
                    camBegin = 0
                if camEnd is None:
                    camEnd = 360

                if camEnd < camBegin:  # The cam is over the axis modulo: display it in two parts
                    bg = pg.BarGraphItem(x=[-camId], y0=camBegin, y1=360, width=0.9, brush=color)
                    bg.rotate(-90)
                    self.ui.graphicsView.addItem(bg)

                    bg = pg.BarGraphItem(x=[-camId], y0=0, y1=camEnd, width=0.9, brush=color)
                    bg.rotate(-90)
                    self.ui.graphicsView.addItem(bg)
                else:  # Standard cam : display it in one part
                    bg1 = pg.BarGraphItem(x=[-camId], y0=camBegin, y1=camEnd, width=0.9, brush=color)
                    bg1.rotate(-90)
                    self.ui.graphicsView.addItem(bg1)

                # Add cam name to list to display as y-axis value
                yAxisNames.append(f"{camName}\n[{camBegin if camBegin else '-∞'}; {camEnd if camEnd else '∞'}]")
            else:  # Cam is not related to master virtual axis
                # In this case, camBegin represents the minimum acceptable y-value
                # while camEnd represents the maximum one
                # => The cam will display areas where axis y-values are in the range [camBegin; camEnd]

                # Get x-values from camAxis where their y-values satisfies the given condition
                # These values can be splitted in multiple area, depending on camAxis shape
                areaStr = f"{camName}\n"
                for area in camAxis.getRange(camBegin, camEnd):
                    bg1 = pg.BarGraphItem(x=[-camId], y0=area[0], y1=area[1], width=0.9, brush=color)
                    bg1.rotate(-90)
                    self.ui.graphicsView.addItem(bg1)

                    areaStr += f" [{area[0]:.2f}; {area[1]:.2f}]"
                # Add cam name to list to display as y-axis value
                yAxisNames.append(areaStr)

        # Add cam names as y-axis values
        yax = self.ui.graphicsView.getAxis('left')
        yax.setTicks([list(zip(range(len(yAxisNames)), yAxisNames))])

        # Set x-axis settings
        xax = self.ui.graphicsView.getAxis('bottom')
        xax.setTickSpacing(20, 20)

        plot = self.ui.graphicsView.getPlotItem()
        # Show grid
        plot.showGrid(True, True, 0.3)
        # Disable mouse interaction
        plot.setMouseEnabled(False, False)
        plot.setMenuEnabled(False)
        # Set view limits
        plot.setXRange(0, 365, 0)

        if not self.ui.dockWidget.isVisible():
            self.ui.dockWidget.show()

    def exportPlot(self):
        filename, _ = QtGui.QFileDialog.getSaveFileName(self.ui.centralwidget,
                                                        caption="Save image as ...",
                                                        filter="PNG files (*.png)")
        if filename:
            exporter = pg.exporters.ImageExporter(self.ui.graphicsView.plotItem)
            exporter.export(filename)


if __name__ == '__main__':
    import sys

    app = QtGui.QApplication(sys.argv)
    w = CamTableViewer()
    w.show()
    sys.exit(app.exec_())
