# -*- coding: utf-8 -*-

from lxml import etree
import logging


class BoschXmlCamParser:
    def __init__(self, file):
        self._file = file

        self._logger = logging.getLogger("app.BoschXmlCamParser")

        self._camShaftDistance = 0
        self._numCamPoints = 1024
        self._lastContainsLastPoint = True
        self._masterModulo = 360
        self._masterStartPos = 0

        self.axisData = {}

    def setCamShaftDistance(self, value):
        self._camShaftDistance = float(value)
        self.axisData['CamShaftDistance'] = self._camShaftDistance

    def setNumCamPoints(self, value):
        self._numCamPoints = int(value)
        self.axisData['NumCamPoints'] = self._numCamPoints

    def setLastContainsLastPoint(self, value):
        if type(value) is not bool:
            raise ValueError("LastContainsLastPoint must be of type bool, not {}".format(type(value)))

        self._lastContainsLastPoint = value
        self.axisData['LastContainsLastPoint'] = value

    def setMasterModulo(self, value):
        self._masterModulo = int(value)
        self.axisData['MasterModulo'] = self._masterModulo

    def setMasterStartPos(self, value):
        self._masterStartPos = int(value)
        self.axisData['MasterStartPosition'] = self._masterStartPos

    def parse(self):
        tree = etree.parse(self._file)

        self.axisData = {}

        try:
            for element in tree.xpath("/CamSaver/AdditionalProfileData")[0].iterchildren():
                if element.tag == "LastCamShaftDistance":
                    self.setCamShaftDistance(element.text)
                elif element.tag == "LastNumCamPoints":
                    self.setNumCamPoints(element.text)
                elif element.tag == "LastContainsLastPoint":
                    self.setLastContainsLastPoint(element.text.lower() == "true")

            for element in tree.xpath("/CamSaver/ProfileData")[0].iterchildren():
                if element.tag == "MasterModulo":
                    self.setMasterModulo(element.text)
                elif element.tag == "MasterStartPosition":
                    self.setMasterStartPos(element.text)
                elif element.tag == "MotionSteps":
                    slaveHubs = []
                    for child in element.iterchildren():  # iterate over MotionStepSaver elements
                        for childElement in child.iterchildren():
                            if childElement.tag == "SlaveHub":
                                slaveHubs.append(float(childElement.text))

                    hubMax = None
                    hubMin = None
                    valueSum = None
                    for value in slaveHubs:
                        if valueSum is None:
                            valueSum = value
                            hubMax = value
                            hubMin = value

                        if valueSum > hubMax:
                            hubMax = valueSum
                        if valueSum < hubMin:
                            hubMin = valueSum
                        valueSum += value

                    if len(slaveHubs):
                        self.setCamShaftDistance(hubMax - hubMin)

            self.axisData['SlavePositionArray'] = []
            xValue = 0
            incrementDenominator = self._numCamPoints if not self._lastContainsLastPoint else self._numCamPoints - 1
            increment = round(self._masterModulo / float(incrementDenominator), 6)
            maxValue = -self._camShaftDistance if self._camShaftDistance > 0 else self._camShaftDistance
            for element in tree.xpath("/CamSaver/SlavePositionArray")[0].iterchildren():
                if element.tag == "double":
                    yValue = float(element.text)
                    if yValue > maxValue:
                        maxValue = yValue
                    self.axisData['SlavePositionArray'].append({'x-pos': xValue,
                                                                'y-pos': yValue})
                else:
                    self._logger.error("Unknown SlavePositionArray tag {}".format(element.tag))
                    return False

                xValue += increment

            self.axisData['yMaxValue'] = maxValue
        except (IndexError, ValueError, TypeError) as e:
            self._logger.error("Parsing error: {}".format(e))
            return False

        return self.axisData

    def getSlavePositions(self, camShaftDistance=None):
        """

        :return: a tuple of two lists which contains x-values and y-values ([x-values], [y-values])
        """
        xPos = []
        yPos = []

        if camShaftDistance is None:
            camShaftDistance = self.axisData['CamShaftDistance']
        coef = 100 / self.axisData['yMaxValue']
        for pos in self.axisData['SlavePositionArray']:
            xPos.append(pos['x-pos'])
            yPos.append(round(coef * pos['y-pos'] * camShaftDistance / 100, 6))
        return xPos, yPos


if __name__ == '__main__':
    import pyqtgraph as pg

    # parser = BoschXmlCamParser("E:\Programmation\workspace\CamTableViewer\sandbox\Cames exemple\LE_MainAxis.xml")
    parser = BoschXmlCamParser("E:\Programmation\workspace\CamTableViewer\sandbox\Cames exemple\LE_StripClampAxis.xml")
    # parser = BoschXmlCamParser("E:\Programmation\workspace\CamTableViewer\sandbox\Cames exemple\LE_StripForward.xml")
    data = parser.parse()
    x, y = parser.getSlavePositions()

    print(data['CamShaftDistance'])
    print(x)
    print(y)

    pg.plot(x, y)

    pg.QtGui.QApplication.exec_()
